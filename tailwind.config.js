/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: 'class',
    content: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            display: ['group-focus'],

            colors: {
                'purpl': '#AC42FF',
                'purplclassic': '#8F00FF',
                'purplc': '#EFDAFF',
                'notclick': '#E6C7FF',
                'click': '#6A09B7',
                'darkmod': '#110D14',
                'purpld': '#261235',
                'btnfooter': '#D9D9D9',
                'textfooter': '#7C7C7C',
                'linkedin': '#0077b5',
                'insta': '#E1306C',
            },
            boxShadow: {
                btn: '0px 2px 1px #8F00FF',
                activedbtn: 'inset 0px 4px 4px rgba(0, 0, 0, 0.51)',
            }
        },
    },
    plugins: [],
}